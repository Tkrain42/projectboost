﻿Shader "Custom/Hologram" {
	Properties{
		_Diffuse("Diffuse Texture", 2D) = "white" {}
		_Normal("Normal Map", 2D) = "bump" {}
		_NormalIntensity("Intensity", Range(0,10)) = 1
		_RimColor("Rim Color", Color) = (0, 0.5, 0.5, 0)
		_RimPower("Rim Power", Range(0.5, 8.0))=3.0
		_RimCutoff("Rim Cutoff", Range (0, 1)) = 0.8
	}
		SubShader{
			Tags{"Queue" = "Transparent"}

			Pass {
			   ZWrite On
			   ColorMask 0
			}


			CGPROGRAM

			#pragma surface surf Lambert alpha:fade



			float4 _RimColor;
			float _RimPower;
			float _RimCutoff;
			sampler2D _Normal;
			sampler2D _Diffuse;
		
		half _NormalIntensity;


		struct Input {
			float3 viewDir;
			float3 worldPos;
			float2 uv_Normal;
			float2 uv_Diffuse;
		};




		void surf (Input IN, inout SurfaceOutput o) {
			o.Normal = UnpackNormal(tex2D(_Normal, IN.uv_Normal));
			o.Normal *= float3(_NormalIntensity, _NormalIntensity, 1);
			half rim = 1-saturate(dot(normalize(IN.viewDir), o.Normal));
			half fixrim = rim > _RimCutoff ? rim : 0;
			o.Emission = _RimColor.rgb * pow(fixrim, _RimPower);
			o.Alpha = pow(rim, _RimPower);
			o.Albedo = tex2D(_Diffuse, IN.uv_Diffuse).rgb;
			
		}
		ENDCG
	}
	FallBack "Diffuse"
}
