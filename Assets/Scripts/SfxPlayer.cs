﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SfxPlayer : MonoBehaviour {
    [SerializeField]
    private AudioClip[] clips;
    private AudioSource audiosource;

    // Use this for initialization
    void Start () {
        audiosource = GetComponent<AudioSource>();
	}
    private void PlayClip(AudioClip clip)
    {
        if (clip)
        {
            audiosource.PlayOneShot(clip);
        }
    }

    public void PlayRandomAudioClip()
    {
        if (audiosource)  //Protection against designer failure.
            if (clips.Length > 0)
            {
                PlayClip(clips[Random.Range(0, clips.Length)]);
            }
    }
    // Update is called once per frame

}
