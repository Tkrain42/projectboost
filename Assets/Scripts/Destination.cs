﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destination : MonoBehaviour {

    [Tooltip("Enter the name of the level when the ship lands on this pad.")]
    public string DestinationLevel = "Level0";
    [SerializeField]
    private string message = "You've completed this level!";
    [SerializeField]
    private int scoreModifier = 500;
    [SerializeField]
    private Color startColor = Color.blue;
    [SerializeField]
    private Color endColor = Color.yellow;
    
    [SerializeField]
    ScriptableInt sharedScore;

    [Range(0, 10)]
    [SerializeField]
    private float speed = 1f;
    private Color currentColor;
    private float runner;
    float deltaR;
    float deltaG;
    float deltaB;

    private void Start()
    {
        currentColor = startColor;
        deltaR = startColor.r - endColor.r;
        deltaG = startColor.g - endColor.g;
        deltaB = startColor.b - endColor.b;
    }

    public void onRocketLanded()
    {
        MessageSystem.SendUIMessage(new Message("toast", message));
        sharedScore.Value += scoreModifier;
    }

    private void Update()
    {
        runner += Time.deltaTime * speed;
        float factor = Mathf.Sin(runner) / 2f + .5f;
        currentColor.r = startColor.r - deltaR * factor;
        currentColor.g = startColor.g - deltaG * factor;
        currentColor.b = startColor.b - deltaB * factor;
        GetComponent<MeshRenderer>().material.color = currentColor;
    }
}
