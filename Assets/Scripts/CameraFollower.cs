﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour {

    protected Rocket rocket;
	// Use this for initialization
	void Start () {
        rocket = FindObjectOfType<Rocket>();
	}
	
	// Update is called once per frame
	void Update () {
        if (rocket)
        {
            Vector3 currentposition = transform.position;
            currentposition.x = rocket.transform.position.x;
            transform.position = currentposition;
        }
	}
}
