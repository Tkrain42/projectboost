﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour {

    [SerializeField]
    private Vector3 movementVector; 

    [Range(.001f, 2)]
    [SerializeField]
    private float movementSpeed = 1.0f;
    [SerializeField]
    private bool Center = true;

    //Use a property to expose Speed to code, to avoid issues with zero speed.
    public float Speed
    {
        get
        {
            return movementSpeed;
        }
        set
        {
            movementSpeed = value;
            if(movementSpeed<=Mathf.Epsilon)
            {
                movementSpeed = Mathf.Epsilon * 2;
            }
        }
    }

    float currentFactor;

    Vector3 startingPosition;
	// Use this for initialization
	void Start () {
        startingPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        currentFactor += Time.deltaTime * movementSpeed;
        float movementFactor = Mathf.Sin(currentFactor);
        Vector3 offset = movementVector * movementFactor;
        transform.position = startingPosition + offset;
	}

    float CalcMovementFactor()
    {
        float result;
        if (Center)
        {
            result = Mathf.Sin(currentFactor);
        }
        else
        {
            result = (Mathf.Sin(currentFactor / 2) + .5f);
        }
        return result;
    }

}
