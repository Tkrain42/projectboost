﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketManager : MonoBehaviour {

    public static RocketManager instance = null;
    public static Rocket rocket = null;
    [Range(1,3000)]
    [SerializeField] float thrust = 1000f;
    [Range(1,300)]
    [SerializeField] float turnSpeed = 75f;
    /// <summary>
    /// Used to determine if a ship is landing too "fast"... so even a safe platform can kill the rocket.
    /// </summary>
    [Range(0,100)]
    [SerializeField] float maxSustainableCollision = 10f;
    [Range(0, 10)]
    [SerializeField]
    float levelLoadDelay = 2.0f;
    [Range(0,20)]
    [SerializeField]
    int livesRemaining = 2;
    [Range(0,20)]
    [SerializeField]
    int maxLives = 5;
    int keysCollected=0;
    int score = 0;


    float startingThrust;
    float startingTurnSpeed;
    float startingMaxSustainableCollision;
    int startingLivesRemaining;

    public bool noMoreLives
    {
        get
        {
            return livesRemaining < 0;
        }
    }

    public int LivesRemaining
    {
        get
        {
            return livesRemaining;
        }
        set
        {
            livesRemaining = Mathf.Clamp(value, -1, maxLives);
            print("Lives Remaining: " + livesRemaining.ToString());
            MessageSystem.SendUIMessage(new Message("lives", "Lives Remaining: "+livesRemaining,-1f));
        }
    }

    public float Thrust
    {
        get
        {
            return thrust;
        }
        set
        {
            thrust = Mathf.Clamp(value, 1, 3000);
            if(rocket)
            {
                rocket.SetThrust(thrust);
            }
        }
    }

    public float TurnSpeed
    {
        get
        {
            return turnSpeed;
        }
        set
        {
            turnSpeed = Mathf.Clamp(value, 1, 30);
            if (rocket)
            {
                rocket.SetTurnSpeed(turnSpeed);
            }
        }
    }

    public float MaxSustainableCollision
    {
        get
        {
            return maxSustainableCollision;
        }
        set
        {
            maxSustainableCollision = Mathf.Clamp(value, 0, 100);
            if (rocket)
            {
                rocket.setMaxSustainableCollision(maxSustainableCollision);
            }
        }
    }


    public int KeysCollected {
        get
        {
            return keysCollected;
        }
        set
        {
            keysCollected = MinZed(value);
        }
    }

    public int Score
    {
        get
        {
            return score;
        }
        set
        {
            score = MinZed(value);
            MessageSystem.SendUIMessage(new Message("score", "Score: "+score.ToString()+" "));
        }
    }

    int MinZed(int value)
    {
        if(value<0)
        {
            return 0;
        } else
        {
            return value;
        }
    }

    float MinZed(float value)
    {
        if (value <= Mathf.Epsilon)
        {
            return 0.0f;
        }
        else
        {
            return value;
        }
    }

    // Use this for initialization
    void Awake () {
        if (RocketManager.instance!=null)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            startingThrust = thrust;
            startingTurnSpeed = turnSpeed;
            startingMaxSustainableCollision = maxSustainableCollision;
            startingLivesRemaining = livesRemaining;
            RocketManager.instance = this;
        }
	}

    private void Start()
    {
        Reset();
    }

    public void Reset()
    {
        thrust = startingThrust;
        turnSpeed = startingTurnSpeed;
        maxSustainableCollision = startingMaxSustainableCollision;
        LivesRemaining = startingLivesRemaining;
        //Score = 0;
        keysCollected = 0;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
