﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//
public class Door : MonoBehaviour {

    [SerializeField]
    [Tooltip("Name of the key that unlocks this door.")]
    protected string Key = "Key";

    [SerializeField]
    [Tooltip("Number of keys required to collect before the door opens.")]
    protected int KeysRequired = 1;

    [SerializeField]
    [Tooltip("How fast the door opens/dissappears.")]
    protected float DoorSpeed = 1.0f;

    [SerializeField]
    [Tooltip("Points for opening this door.")]
    protected int scoreModifier = 100;
    [SerializeField]
    protected ScriptableInt sharedScore;
    protected int KeysCollected = 0;
    
    protected bool isTriggered
    {
        get
        {
            return (KeysCollected >= KeysRequired);
        }
    }

    // Use this for initialization
    void Start() {

    }

    public void OnKeyCollected(string KeyName)
    {
        if (isTriggered)
            return;
        if (Key == KeyName)
        {
            
            KeysCollected++;
            
            if (KeysCollected >= KeysRequired)
            {
                MessageSystem.SendUIMessage(new Message("toast", "Opening Door!", 1f));
                sharedScore.Value += scoreModifier;
                StartCoroutine(OpenDoor());
            }
        }
    }

    public virtual IEnumerator OpenDoor()
    {
        for (int x = 0; x < 30; x++)
        {
            transform.localScale = transform.localScale * .95f;
            yield return new WaitForSeconds(DoorSpeed / 30);
        }
        Destroy(gameObject);
    }

	// Update is called once per frame
	void Update () {
		
	}
}
