﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Globals : MonoBehaviour {

    public const string TYPE_FRIENDLY = "Friendly";
    public const string TYPE_FUEL = "Fuel";
    public const string TYPE_SCORE = "Score";
    public const string TYPE_FINISH = "Finish";
    public const string TYPE_POWERUP = "Powerup";


}
