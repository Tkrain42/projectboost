﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerThrust : Powerup {


    [SerializeField] float thrustGain=20.0f;
    // Use this for initialization

    public override void onPowerupCollected()
    {
        if (triggered)
            return;
        RocketManager.instance.Thrust += thrustGain;
        MessageSystem.SendUIMessage(new Message("toast", "Thrust boosted by " + thrustGain + " units."));
        base.onPowerupCollected();
    }

}
