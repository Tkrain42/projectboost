﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerTurnSpeed : Powerup {

    [SerializeField] SharedData sharedData;
    [SerializeField] float turnSpeedGain = 20.0f;
    // Use this for initialization

    public override void onPowerupCollected()
    {
        if (triggered)
            return;
        RocketManager.instance.TurnSpeed += turnSpeedGain;
        sharedData.turnSpeed += turnSpeedGain;
        MessageSystem.SendUIMessage(new Message("toast", "Turn Speed boosted by " + turnSpeedGain + " units."));
        base.onPowerupCollected();
    }
}
