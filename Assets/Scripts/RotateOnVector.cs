﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOnVector : MonoBehaviour {
    [SerializeField]
    Vector3 rotation;
    [Range(0,10)]
    [SerializeField]
    float speed = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        print("Update");
        transform.Rotate(rotation * speed * Time.deltaTime);
	}
}
