﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

[RequireComponent (typeof(AudioSource))]
public class DoorKey : MonoBehaviour {

    [SerializeField]
    [Tooltip("Name of this key.  Must match the door's Key name")]
    string keyName = "Key";
    [SerializeField]
    int scoreModifier = 10;
    [SerializeField] ScriptableInt sharedScore;
    [SerializeField]
    private AudioClip[] clips;
    bool triggered = false;
    AudioSource audiosource;

    public string GetKey
    {
        get
        {
            return keyName;
        }
    }

	// Use this for initialization
	void Start () {
        audiosource = GetComponent<AudioSource>();
	}
	
    public void TriggerKey()
    {
        if (!triggered)
        {
            PlayRandomAudioClip();
            DoBroadcast();
            triggered = true;
            sharedScore.Value += scoreModifier;
            RocketManager.instance.KeysCollected += 1;
            Destroy(GetComponent<Collider>());
            Destroy(GetComponent<MeshFilter>());
            Invoke("Die", 1f);
        }
    }

   

    private void PlayClip(AudioClip clip)
    {
        if (clip)
        {
            audiosource.PlayOneShot(clip);
        }
    }

    private void PlayRandomAudioClip()
    {
        if (audiosource)  //Protection against designer failure.
            if (clips.Length > 0)
            {
                PlayClip(clips[Random.Range(0, clips.Length)]);
            }
    }

    void DoBroadcast()
    {    
            var allDoors = FindObjectsOfType<Door>();
            foreach (Door mb in allDoors)
            {
            mb.BroadcastMessage("OnKeyCollected", keyName);
                }
    }

    void Die()
    {
        Destroy(gameObject);
    }

}
