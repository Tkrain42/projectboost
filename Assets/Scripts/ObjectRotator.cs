﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour {

    [SerializeField]
    float RotationSpeed = 10f;
    [SerializeField]
    float ZRotSpeed = 0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(RotationSpeed * Vector3.down * Time.deltaTime);
        transform.Rotate(ZRotSpeed * Vector3.left * Time.deltaTime);
	}
}
