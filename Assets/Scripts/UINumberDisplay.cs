﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Text))]
public class UINumberDisplay : MonoBehaviour {


    [SerializeField] ScriptableInt sharedNumber;
    [SerializeField] string caption = "";
    Text text;

    private void OnEnable()
    {
        sharedNumber.onIntValueChanged += onIntValueChanged;
    }

    private void OnDisable()
    {
        sharedNumber.onIntValueChanged -= onIntValueChanged;
    }


    void onIntValueChanged(){
        text.text = caption + sharedNumber.Value;
    }


    void Start () {
        text = GetComponent<Text>();
        text.text = caption + sharedNumber.Value;
	}
	


}
