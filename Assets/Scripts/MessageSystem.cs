﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Handler class for the MessageSystem  
/// </summary>
public class Message
{
    /// <summary>
    /// Tag of receiving MessageSystem object.  Only MessageSystem objects with this messageTag will receive the message.
    /// </summary>
    public string tag;     //The tag of the MessageSystem that should display the text.
    /// <summary>
    /// The actual text the MessageSystem object should display.
    /// </summary>
    public string text;    //The actual text to display.
    /// <summary>
    /// The duration of the message.  Set to negative number for infinite duration.
    /// </summary>
    public float duration; //The duration of the message.  Set to negative number for infinte duration.

    /// <summary>
    /// New message with permanent duration.
    /// </summary>
    /// <param name="tag"></param>
    /// <param name="message"></param>
    public Message(string tag, string message)
    {
        this.tag = tag; 
        text = message; 
        duration = -1f;
    }
    /// <summary>
    /// New Message with specified duration.
    /// </summary>
    /// <param name="tag"></param>
    /// <param name="message"></param>
    /// <param name="duration"></param>
    public Message(string tag, string message, float duration)
    {
        this.tag = tag; 
        this.text = message; 
        this.duration = duration;  
    }
}
[RequireComponent(typeof(Text))]
public class MessageSystem : MonoBehaviour {

    [SerializeField] protected string messageTag = "default";
    [SerializeField] protected string defaultText = "";
    protected Text ourText;
    protected float duration = -1f;
    protected float currentDuration = 0f;

	// Use this for initialization
	void Start () {
        ourText = GetComponent<Text>();
        ourText.text = "";
	}
	/// <summary>
    /// Sends a message using a Message() component.
    /// </summary>
    /// <param name="message"></param>
    public static void SendUIMessage(Message message)
    {
        var messageSystems = FindObjectsOfType<MessageSystem>();
        foreach(MessageSystem system in messageSystems)
        {
            system.BroadcastMessage("OnUIMessage", message);
        }
    }
    /// <summary>
    /// Sends a message to MessageSystems with tag and message, with the default duration of -1
    /// </summary>
    /// <param name="tag">Tag of receiving object</param>
    /// <param name="message">Actual message to send</param>
    public static void SendUIMessage(string tag, string message)
    {
        SendUIMessage(new Message(tag, message));
    }

    /// <summary>
    /// Sends message to MessageSystems with tag and message, with specified duration.
    /// </summary>
    /// <param name="tag">Tag of receiving object</param>
    /// <param name="message">Actual message to send</param>
    /// <param name="duration">Duration of message.  -1 for permanent.</param>
    public static void SendUIMessage(string tag, string message, float duration)
    {
        SendUIMessage(new Message(tag, message, duration));
    }


    /// <summary>
    /// Clears message on any MessageSystems with tag.
    /// </summary>
    /// <param name="tag">Tag of receiving object.</param>
    public static void SendUIMessage(string tag)
    {
        SendUIMessage(new Message(tag, "", -1));
    }
    public void OnUIMessage(Message message)
    {
        if (ourText)
        {
            if (message.tag == this.messageTag)
            {
                ourText.text = message.text;
                duration = message.duration;
                currentDuration = 0f;
            }
        }
    }

	// Update is called once per frame
	void Update () {
        if (duration < 0.0f)
            return;
        currentDuration += Time.deltaTime;
        if (currentDuration > duration)
        {
            ourText.text = "";
            duration = -1.0f;
        }
	}
}
