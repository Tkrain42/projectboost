﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorRotate : Door {

    [SerializeField] protected Vector3 rotationVector;
    [SerializeField] protected bool destroyOnOpen = true;

	// Use this for initialization
	void Start () {
		
	}

    public override IEnumerator OpenDoor()
    {
        Vector3 deltaVector = rotationVector / 30.0f;
        for (int x = 0; x < 30; x++)
        {
            transform.Rotate(deltaVector);
            yield return new WaitForSeconds(DoorSpeed / 30);
        }
        if (destroyOnOpen)
        {
            Destroy(gameObject);
        }

    }

    // Update is called once per frame
    void Update () {
		
	}
}
