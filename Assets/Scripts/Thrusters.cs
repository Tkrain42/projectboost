﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Thrusters : MonoBehaviour {

    [SerializeField]
    ScriptableFloat thrust;
    [SerializeField]
    ScriptableFloat turnSpeed;
    [SerializeField]
    ScriptableBool thrusting;
    [SerializeField]
    ScriptableBool alive;
    new Rigidbody rigidbody;

    Vector3 turnLeft
    {
        get
        {
            return -Vector3.forward * turnSpeed.Value;
        }
    }

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        print("FixedUpdate");
        ProcessThrusters();
        ProcessRotators();
    }

    private void ProcessThrusters()
    {
        print("ProcessThrusters");
        if (alive.Value)
        {
            print("alive=true");
            if (CrossPlatformInputManager.GetButton("Jump"))
            {
                rigidbody.AddRelativeForce(Vector3.up * thrust.Value * Time.deltaTime);
                thrusting.Value = true;
            }
            else if (thrusting.Value)
            {
                thrusting.Value = false;
            }
        }
        else
        {
            print("alive=false");
        }

    }

    private void ProcessRotators()
    {
        //Release the physics rotation while we navigate.
        rigidbody.freezeRotation = true;
        transform.Rotate(turnLeft * CrossPlatformInputManager.GetAxis("Horizontal") * Time.deltaTime);
        //resume physics rotation now that movement is satisfied.
        rigidbody.freezeRotation = false;
    }
}
