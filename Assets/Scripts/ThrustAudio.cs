﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrustAudio : MonoBehaviour {

    [SerializeField] ScriptableBool thrusting;
    [SerializeField] ScriptableBool alive;
    [SerializeField] ScriptableAudioClips audioClips;
    bool soundIsPlaying = false;
    AudioSource audioSource;
    // Use this for initialization

    private void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();
        thrusting.onBoolChanged += onThrustChanged;
        alive.onBoolChanged += onAliveChanged;
    }

    void onThrustChanged()
    {
        if (!alive.Value)
            return;
        if (thrusting.Value)
        {
            StartThrusting();
        }
        else
        {
            StopThrusting();
        }
    }

    void onAliveChanged()
    {
        if (!alive.Value)
        {
            StopThrusting();
        }
    }



    void StopThrusting()
    {
        audioSource.Stop();
        soundIsPlaying = false;
    }

    void StartThrusting()
    {
        if(!soundIsPlaying)
            audioSource.PlayOneShot(audioClips.RandomClip);
        soundIsPlaying = true;
    }

}
