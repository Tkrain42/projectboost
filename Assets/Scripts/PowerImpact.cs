﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerImpact : Powerup {

    [Range(1,20)]
    [Tooltip("Increases maxSustainableCollision by this amount.  Note that this only applies to collisions with friendly objects.")]
    [SerializeField] float maxImpactGain = 1;
    // Use this for initialization

    public override void onPowerupCollected()
    {
        if (triggered)
            return;
        RocketManager.instance.MaxSustainableCollision += maxImpactGain;
        MessageSystem.SendUIMessage(new Message("toast", "Durability increased."));
        base.onPowerupCollected();
    }
}
