﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSlider : Door {

    [SerializeField]
    protected Vector3 moveVector;
    [SerializeField]
    protected bool destroyOnOpen=true;


	// Use this for initialization
	void Start () {
		
	}
	
    public override IEnumerator OpenDoor()
    {
        Vector3 deltaVector = moveVector / 30.0f;
        for (int x = 0; x < 30; x++)
        {
            transform.position+=deltaVector;
            yield return new WaitForSeconds(DoorSpeed / 30);
        }
        if (destroyOnOpen)
        {
            Destroy(gameObject);
        }
        
    }

    // Update is called once per frame
    void Update () {
		
	}
}
