﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityStandardAssets.CrossPlatformInput;

/// <summary>
/// The main player class.
/// </summary>
public class Rocket : MonoBehaviour {

    [SerializeField]
    SharedData sharedData;

    /// <summary>
    /// This figure will be multiplied by Time.deltatime to add accelleration.
    /// </summary>
    [SerializeField] float thrust = 15f;
    [SerializeField] float turnSpeed = 10f;
    /// <summary>
    /// Used to determine if a ship is landing too "fast"... so even a safe platform can kill the rocket.
    /// </summary>
    [SerializeField] float maxSustainableCollision = 10f;
    [SerializeField] ScriptableFloat sharedThrust;
    [SerializeField] ScriptableFloat sharedTurnSpeed;
    [SerializeField] ScriptableFloat sharedMaxCollision;
    [SerializeField] ScriptableInt sharedScore;
    [SerializeField] ScriptableInt sharedLives;

    [SerializeField] float shipSpeed;
    [Range(0,10)]
    [SerializeField] float levelLoadDelay = 2.0f;
    [SerializeField] AudioClip audMainEngine;
    [SerializeField] AudioClip audSuccess;
    [SerializeField] AudioClip audDeath;

    [SerializeField] ParticleSystem prtMainEngine;
    [SerializeField] ParticleSystem prtSuccess;
    [SerializeField] ParticleSystem prtDeath;

    bool pressingLeft;
    bool pressingRight;
    bool isThrusting;
    bool debugNoCrashing = false;
    bool isAlive = true;
    new Rigidbody rigidbody;
    AudioSource audiosource;
    //public static Rocket instance = null;
    
    enum PlayerState { Alive, Dying, Transcending}
    PlayerState playerstate;

    private Vector3 turnRight
    {
        get
        {
            return Vector3.forward * sharedTurnSpeed.Value;
        }
    }
    private Vector3 turnLeft
    {
        get
        {
            return -(Vector3.forward * sharedTurnSpeed.Value);
        }
    }

    [SerializeField]
    float ExplosionForce=100f;

    string NextScene;

    // Use this for initialization
    void Start ()
    {
        {
            Initialize();

           // Rocket.instance = this;
        }
    }

    /// <summary>
    /// Basic initialization of the Rocket class.  This was originally extracted to better support a singleton model, but the singleton idea is currently on hold.
    /// </summary>
    private void Initialize()
    {

        RocketManager.rocket = this;
        rigidbody = GetComponent<Rigidbody>();
        audiosource = GetComponent<AudioSource>();

        playerstate = PlayerState.Alive;
        Invoke("ForceRefresh", .05f);

    }

    private  void ForceRefresh()
    {

    }



    /// <summary>
    /// Intended for using the Singleton pattern, pass the static instance the transform so that the singleton can start in the proper location.
    /// </summary>
    /// <param name="newTransform">Passed by the new rocket to the singleton before the new rocket destroys itself.</param>
    public void ResetInstanceRocket(Transform newTransform)
    {
        transform.position = newTransform.position;
        transform.rotation = newTransform.rotation;

        Initialize();     
    }

	// Update is called once per frame
	void Update () {
  
	}

    void FixedUpdate()
    {
        if (playerstate == PlayerState.Alive)
        {
            Thrusters();
            Rotators();

            if (Debug.isDebugBuild)
            {
                DebugKeys(); 
            }

        }
    }

    private void DebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.C)){
            debugNoCrashing = !debugNoCrashing;
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            SceneManager.LoadScene ((SceneManager.GetActiveScene().buildIndex+1) % SceneManager.sceneCountInBuildSettings);
        }
    }

    private void OnDrawGizmos()
    {
        if (debugNoCrashing)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, 2f);
        }
    }



    private void Rotators()
    {
        //Release the physics rotation while we navigate.
        rigidbody.freezeRotation = true;
        transform.Rotate(turnLeft * CrossPlatformInputManager.GetAxis("Horizontal") * Time.deltaTime);
        //resume physics rotation now that movement is satisfied.
        rigidbody.freezeRotation = false;
    }

    private void Thrusters()
    {
        if (CrossPlatformInputManager.GetButton("Jump"))
        {
            rigidbody.AddRelativeForce(Vector3.up * sharedThrust.Value *Time.deltaTime);
            PlayThrusterSound();
            if (!debugNoCrashing) prtMainEngine.Play();
        }
        else if (isThrusting)
        {
            StopThrusterSound();
            prtMainEngine.Stop();
        }
        
    }

    private void StopThrusterSound()
    {
        audiosource.Stop();
        isThrusting = false;
    }

    private void PlayThrusterSound()
    {
        if (!audiosource.isPlaying)
            audiosource.PlayOneShot(audMainEngine);
        isThrusting = true;
    }

    private void PlayCrashSound()
    {
        audiosource.PlayOneShot(audDeath);
    }

    private void PlayLandingSound()
    {
        audiosource.PlayOneShot(audSuccess);
    }

    void OnCollisionEnter(Collision collision)
    {

        if (playerstate != PlayerState.Alive)
        {
            return;
        }
        
        switch (collision.gameObject.tag)
        {
            case Globals.TYPE_FRIENDLY:
                CollisionFriendly(collision);
                break;
            case Globals.TYPE_FUEL:
                CollisionFuel(collision);
                break;
            case Globals.TYPE_FINISH:
                CollisionFinish(collision);
                break;

                
            default:
                MessageSystem.SendUIMessage(new Message("toast", "Avoid obstacles!"));
                KillPlayer(collision);
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (playerstate != PlayerState.Alive)
        {
            return;
        }
        switch (other.gameObject.tag)
        {
            case Globals.TYPE_SCORE:
                CollissionScore(other.gameObject);
                break;
            case Globals.TYPE_POWERUP:
                CollisionPowerup(other.gameObject);
                break;
            default:
                break;
        }
    }

    void CollisionPowerup(GameObject other)
    {
        Powerup powerup = other.gameObject.GetComponent<Powerup>();
        if (powerup)
        {
            powerup.onPowerupCollected();
        }
    }

    /// <summary>
    /// Compares the relative velocities between this object and the collision object.  If speed is greater than <code>maxSustainableCollision</code> 
    /// then true is returned and the ship should be destroyed even if hitting a safe location.
    /// </summary>
    /// <param name="collision"></param>
    /// <returns></returns>

    private bool ShipTooFast(Collision collision)
    {
        shipSpeed = collision.relativeVelocity.magnitude;
        return (collision.relativeVelocity.magnitude > sharedMaxCollision.Value);

    }

    private  void CollissionScore(GameObject other)
    {
        DoorKey doorkey = other.gameObject.GetComponent<DoorKey>();
        if (doorkey)
        {
            doorkey.TriggerKey();
        }
        
    }

    private  void CollisionFinish(Collision collision)
    {
        if (ShipTooFast(collision))
        {
            MessageSystem.SendUIMessage(new Message("toast", "Watch speed when landing on friendly surfaces!"));
            KillPlayer(collision);
        }
        else
        {
            audiosource.Stop();
            PlayLandingSound();
            prtMainEngine.Stop();
            prtSuccess.Play();
            playerstate = PlayerState.Transcending;
            NextScene = collision.gameObject.GetComponent<Destination>().DestinationLevel;
            collision.gameObject.GetComponent<Destination>().onRocketLanded();
            RocketManager.instance.LivesRemaining += 1;
            Invoke("LoadNextScene", levelLoadDelay);
        }
    }

    private void LoadNextScene()
    {
        SceneManager.LoadScene(NextScene);
    }

    private void CollisionFriendly(Collision collision)
    {
        if (ShipTooFast(collision))
        {
            MessageSystem.SendUIMessage(new Message("toast", "Watch speed when landing on friendly surfaces!"));
            KillPlayer(collision);
        }
    }

    private  void CollisionFuel(Collision collision)
    {
        
    }

    private void KillPlayer(Collision collision)
    {
        if (debugNoCrashing)
        {
            MessageSystem.SendUIMessage(new Message("toast", "You should be dead, but you've turned collisions off!", .5f));
            return;
        }
        rigidbody.AddExplosionForce(ExplosionForce+(collision.relativeVelocity.magnitude*25),collision.contacts[0].point,100f);

        
        playerstate = PlayerState.Dying;
        if (isThrusting)
        {
            StopThrusterSound();
            prtMainEngine.Stop();
        }
        audiosource.PlayOneShot(audDeath);
        prtDeath.Play();
        RocketManager.instance.LivesRemaining -= 1;
        if (RocketManager.instance.noMoreLives)
        {
            Invoke("RestartGame", levelLoadDelay);
        } else
        {
            NextScene = SceneManager.GetActiveScene().name;
            Invoke("LoadNextScene", levelLoadDelay);
        }
    }

    private void RestartGame()
    {
        RocketManager.instance.Reset();
        SceneManager.LoadScene("Level1");
    }

    public void SetThrust(float newThrust)
    {
        sharedThrust.Value = newThrust;
    }

    public void SetTurnSpeed(float newTurnSpeed)
    {
        sharedTurnSpeed.Value = newTurnSpeed;
    }

    public void setMaxSustainableCollision(float newMaxSustainableCollision)
    {
        sharedMaxCollision.Value = newMaxSustainableCollision;
    }
}
