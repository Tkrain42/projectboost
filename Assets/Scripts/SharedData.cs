﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SharedData", menuName = "Shared/Data")]
public class SharedData : ScriptableObject {

    public int score = 0;
    public float thrust = 15f;
    public float turnSpeed = 10f;


	

}
