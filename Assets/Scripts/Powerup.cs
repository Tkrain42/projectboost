﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SfxPlayer))]
public class Powerup : MonoBehaviour {

    [SerializeField] SharedData sharedData;
    [SerializeField] ScriptableInt sharedScore;
    [SerializeField]
    protected int scoreModifier = 10;
    protected bool triggered = false;



    /// <summary>
    /// Each actual powerup should override this to create a desired effect.
    /// </summary>
    virtual public void onPowerupCollected()
    {
        if (triggered)
            return;
        GetComponent<SfxPlayer>().PlayRandomAudioClip();
        triggered = true;

        sharedScore.Value += scoreModifier;
        Destroy(GetComponent<Collider>());
        Destroy(GetComponent<MeshFilter>());
        Invoke("Die", 1f);
    }



    virtual public void Die()
    {
        Destroy(gameObject);
    }

}
