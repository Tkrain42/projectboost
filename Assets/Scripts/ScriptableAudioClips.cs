﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SharedAudio", menuName = "Shared/Audio")]
public class ScriptableAudioClips : ScriptableObject {
    [SerializeField] List<AudioClip> clips;

    public AudioClip RandomClip
    {
        get
        {
            return clips[Random.Range(0, clips.Count)];
        }
    }

    public AudioClip Clip(int clipId)
    {
        int clipToFetch = clipId % clips.Count;
        return clips[clipToFetch];
    }

    public int Count
    {
        get
        {
            return clips.Count;
        }
    }
}
