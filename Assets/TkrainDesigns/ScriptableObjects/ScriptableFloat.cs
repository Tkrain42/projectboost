﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SharedFloat", menuName = "Shared/Float")]
public class ScriptableFloat : ScriptableObject {

    [SerializeField]
    float val;
    float backing;
    [SerializeField]
    bool clamp = false;
    [SerializeField]
    float min = float.MinValue;
    [SerializeField]
    float max = float.MaxValue;

    public delegate void OnFloatValueChanged();
    public event OnFloatValueChanged onFloatValueChanged;

    private void OnEnable()
    {
        Debug.LogWarning("OnEnable");
        Reset();
    }

    public float Value
    {
        get
        {
            return backing;
        }
        set
        {
            if (backing!=val)
            {
                backing = clamp ? Mathf.Clamp(value, min, max) : value;
                NotifyListeners(); 
            }
        }
    }

    private void NotifyListeners()
    {
        if (onFloatValueChanged != null)
        {
            onFloatValueChanged();
        }
    }

    public void Reset()
    {
        Value = val;
       
    }
}
