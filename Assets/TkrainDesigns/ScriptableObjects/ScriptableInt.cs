﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SharedInt", menuName = "Shared/Int")]

public class ScriptableInt : ScriptableObject
{

    [SerializeField]
    int val;
    int backing;
    [SerializeField] bool clamp = false;
    [SerializeField] int min = int.MinValue;
    [SerializeField] int max = int.MaxValue;

    public delegate void OnIntValueChanged();
    public event OnIntValueChanged onIntValueChanged;

    void OnEnable()
    {
        Debug.LogWarning("OnEnable");
        Reset();
    }

    public int Value
    {
        get
        {
            return backing;
        }
        set
        {
            if (backing!=val)
            {
                backing = value;
                if (clamp)
                {
                    backing = Mathf.Clamp(value, min, max);
                }
                if (onIntValueChanged != null)
                {
                    onIntValueChanged();
                } 
            }
        }
    }

    void Reset()
    {
        {
            Value = val; 
        }
    }
}