﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "SharedBool", menuName = "Shared/Bool")]
public class ScriptableBool : ScriptableObject {

    [SerializeField] bool val;
    bool backing;

    public bool Value
    {
        get
        {
            return backing;
        }
        set
        {
            if (backing ^ value)
            {
                backing = value;
                NotifyListeners();
            }
        }
    }

    private void NotifyListeners()
    {
        if (onBoolChanged != null)
        {
            onBoolChanged();
        }
    }

    public void Reset()
    {
        if (backing ^ val)
        {
            backing = val;
            NotifyListeners();
        }
    }


    private void OnEnable()
    {
        Debug.LogWarning("Start");
        Reset();
    }
    public delegate void OnBoolChanged();
    public event OnBoolChanged onBoolChanged;

}
